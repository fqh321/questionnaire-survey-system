# 问卷调查系统

#### 介绍
接程序设计，系统代做，调试部署。
基于Springboot,vue2前后端分离的问卷调查系统。
综合课程设计
#### 软件架构
Springboot+vue2前后端分离系统，有独立的后台系统和前台系统。
使用了mybatis,mabatisplus,redius,mysql等技术。


#### 安装教程

1.  通过git导到本地。
2.  vue需要在导入后下载各种包。

#### 使用说明

1.  注意有文件上传和下载的功能，需要更改自己本地文件的上传下载地址。
2.  如果有布置到远程的需求，不需要一个一个的改localhost，只需要将配置yml文件里的ip: localhost注释掉，把上一行的ip: 121.36.227.24打开用你自己的远程地址，同理，            
    vue的配置文件也需要修改。
3.  在登录时账号密码为admin，进入管理员界面，其他的都为普通登录，普通界面和管理员界面是完全不同的。
4.  管理员后台界面还有高德地图功能，需要自己注册，否则地图界面会报错。
5.  为了减轻系统压力，有些界面使用了缓存，需要开启redius，否则可能导致功能不全。
6.  聊天室功能只能说基本实现，界面不太好看，需要登录两个浏览器两个账号可以发消息，刷新。
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

![输入图片说明](image.png)
![输入图片说明](%E9%97%AE%E5%8D%B7%E7%95%8C%E9%9D%A2.png)
![输入图片说明](%E7%BB%9F%E8%AE%A1.png)
![输入图片说明](%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
![输入图片说明](2.png)
![输入图片说明](3.png)
![输入图片说明](%E5%90%8E%E5%8F%B0.png)
![输入图片说明](4.png)
![输入图片说明](6.png)
#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
