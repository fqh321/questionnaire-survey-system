package com.survey.springboot.common;

public enum PaperStatus {
    INIT,       //初始化
    START,     //开始发放
    STOP;      //停止发放

}
