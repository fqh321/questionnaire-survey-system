package com.survey.springboot.service;

import com.survey.springboot.controller.dto.QuestionDTO;
import com.survey.springboot.controller.dto.ResponseDTO;
import com.survey.springboot.entity.Question;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-03
 */
public interface IQuestionService extends IService<Question> {
    ResponseDTO addQuestion(Integer paperId);   //增加问题

    ResponseDTO updateQuestion(QuestionDTO questionVO); //更新呢问题

    ResponseDTO deleteQuestion(Integer questionId); //删除问题
}
