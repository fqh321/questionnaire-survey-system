package com.survey.springboot.service;

import com.survey.springboot.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-02
 */
public interface IMenuService extends IService<Menu> {

    List<Menu> findMenus(String name);
}
