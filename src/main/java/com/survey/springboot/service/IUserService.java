package com.survey.springboot.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.survey.springboot.controller.dto.ResponseDTO;
import com.survey.springboot.controller.dto.UserDTO;
import com.survey.springboot.controller.dto.UserPasswordDTO;
import com.survey.springboot.controller.dto.UserVO;
import com.survey.springboot.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-02
 */
public interface IUserService extends IService<User> {

    UserDTO login(UserDTO userDTO);

    User register(UserDTO userDTO);

    void updatePassword(UserPasswordDTO userPasswordDTO);

    Page<User> findPage(Page<User> objectPage, String username, String email, String address);

    ResponseDTO addUser(UserVO userVO);
    ResponseDTO login2(UserVO userVO);

    int ha(UserDTO userDTO);
}
