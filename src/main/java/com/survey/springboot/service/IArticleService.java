package com.survey.springboot.service;

import com.survey.springboot.entity.Article;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-02
 */
public interface IArticleService extends IService<Article> {

}
