package com.survey.springboot.service;

import com.survey.springboot.controller.dto.AnswerDTO;
import com.survey.springboot.controller.dto.ResponseDTO;
import com.survey.springboot.entity.Answer;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-02
 */
public interface IAnswerService extends IService<Answer> {
    ResponseDTO addAnswers(List<AnswerDTO> answerVOList);   //增加回答
    ResponseDTO reviewAnswers(int paperId); //回顾回答
}
