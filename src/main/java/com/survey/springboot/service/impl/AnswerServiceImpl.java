package com.survey.springboot.service.impl;

import com.survey.springboot.common.PaperStatus;
import com.survey.springboot.controller.dto.AnswerDTO;
import com.survey.springboot.controller.dto.PaperDTO;
import com.survey.springboot.controller.dto.ResponseDTO;
import com.survey.springboot.entity.Answer;
import com.survey.springboot.mapper.AnswerMapper;
import com.survey.springboot.mapper.PaperMapper;
import com.survey.springboot.service.IAnswerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-02
 */
@Service
public class AnswerServiceImpl extends ServiceImpl<AnswerMapper, Answer> implements IAnswerService {
    private final static String EARLY="问卷未开始发放";
    private final static String INVALIDATION="问卷已失效";
    @Autowired
    AnswerMapper answerMapper;
    @Autowired
    PaperMapper paperMapper;

    @Override   //增加回答
    public ResponseDTO addAnswers(List<AnswerDTO> answerVOList) {
        try {
            int paperId=answerVOList.get(0).getPaperId();
            PaperDTO paperVO=paperMapper.selectByPaperId(paperId);
            if(paperVO.getStartTime()!=null && paperVO.getEndTime()!=null){
                if(paperVO.getStatus()== PaperStatus.INIT)
                    return ResponseDTO.buildFailure(EARLY);
                if(paperVO.getStatus()== PaperStatus.STOP)
                    return ResponseDTO.buildFailure(INVALIDATION);
            }
            String uuid = UUID.randomUUID().toString();  //转化为String对象
            uuid = uuid.replace("-", ""); //因为UUID本身为32位只是生成时多了“-”，所以将它们去点就可
            for(AnswerDTO answerVO:answerVOList) {
                answerVO.setUser_uuid(uuid);
                answerMapper.addAnswer(answerVO);
            }
            return ResponseDTO.buildSuccess();
        }catch (Exception e){
            System.out.println(e);
            return ResponseDTO.buildFailure(e.getMessage());
        }
    }

    @Override   //回顾回答
    public ResponseDTO reviewAnswers(int paperId) {
        List res = new ArrayList();
        List<String> users = answerMapper.selectUUIDbyPaper(paperId);
        for (String user_uuid:users){
            List<AnswerDTO> answerVOS = answerMapper.selectAnswersByUUID(user_uuid);
            List<String> ansString = new ArrayList<>();
            ansString.add(answerVOS.get(0).getCreateTime());
            for(AnswerDTO answerVO:answerVOS) {
                if (answerVO.getQuestionType() == 3){
                    ansString.add(answerVO.getAnswerContent());
                }else if (answerVO.getQuestionType()==2){
                    String[] seqs = answerVO.getAnswerContent().split(",");
                    String tempRes = "";
                    for(String seq:seqs){
                        tempRes+=answerMapper.selectOption(answerVO.getQuestionId(),seq);
                        tempRes+=";";
                    }
                    ansString.add(tempRes);
                }else if(answerVO.getQuestionType()==1){
                    ansString.add(answerMapper.selectOption(answerVO.getQuestionId(),answerVO.getAnswerContent()));
                }
            }
            res.add(ansString);
        }
        return ResponseDTO.buildSuccess(res);
    }
}
