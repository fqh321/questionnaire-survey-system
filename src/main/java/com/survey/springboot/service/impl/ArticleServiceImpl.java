package com.survey.springboot.service.impl;

import com.survey.springboot.entity.Article;
import com.survey.springboot.mapper.ArticleMapper;
import com.survey.springboot.service.IArticleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-02
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements IArticleService {

}
