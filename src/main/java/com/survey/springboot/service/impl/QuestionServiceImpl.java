package com.survey.springboot.service.impl;

import com.survey.springboot.controller.dto.OptionsDTO;
import com.survey.springboot.controller.dto.QuestionDTO;
import com.survey.springboot.controller.dto.ResponseDTO;
import com.survey.springboot.entity.Options;
import com.survey.springboot.entity.Question;
import com.survey.springboot.mapper.OptionsMapper;
import com.survey.springboot.mapper.QuestionMapper;
import com.survey.springboot.service.IQuestionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-03
 */
@Service
public class QuestionServiceImpl extends ServiceImpl<QuestionMapper, Question> implements IQuestionService {
    private final static String INVALIDATION="无效ID";
    @Autowired
    QuestionMapper questionMapper;
    @Autowired
    OptionsMapper optionsMapper;

    @Override   //增加问题
    public ResponseDTO addQuestion(Integer paperId) {
        try {     //前端的逻辑是 添加问题时啥也没有是空的 只返回questionId 真正填写完了调updateQuestion
            Question question=new Question();
            question.setPaperId(paperId);
            questionMapper.addQuestion(question);
            return ResponseDTO.buildSuccess(question.getId());
        }catch (Exception e){
            System.out.println(e);
            return ResponseDTO.buildFailure(e.getMessage());
        }
    }

    @Override   //更新呢问题
    public ResponseDTO updateQuestion(QuestionDTO questionVO) {
        try {
            if(questionMapper.selectByQuestionId(questionVO.getId())==null)
                return ResponseDTO.buildFailure(INVALIDATION);
            Question question=new Question();
            BeanUtils.copyProperties(questionVO,question);
            questionMapper.updateQuestion(question);
            for(OptionsDTO options:questionVO.getOptions()){
                OptionsDTO optionsVO=new OptionsDTO();
                BeanUtils.copyProperties(options,optionsVO);
                if(optionsMapper.findOption(optionsVO)==null)
                    optionsMapper.addOption(optionsVO);
                else
                    optionsMapper.updateOption(optionsVO);
            }
            return ResponseDTO.buildSuccess();
        }catch (Exception e){
            System.out.println(e);
            return ResponseDTO.buildFailure(e.getMessage());
        }
    }

    @Override   //删除问题
    public ResponseDTO deleteQuestion(Integer questionId) {
        try {
            if(questionMapper.selectByQuestionId(questionId)==null)
                return ResponseDTO.buildFailure(INVALIDATION);
            questionMapper.deleteQuestion(questionId);
            optionsMapper.deleteByQuestionId(questionId);
            return ResponseDTO.buildSuccess();
        }catch (Exception e){
            System.out.println(e);
            return ResponseDTO.buildFailure(e.getMessage());
        }
    }
}
