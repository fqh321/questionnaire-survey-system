package com.survey.springboot.service.impl;

import com.survey.springboot.common.PaperStatus;
import com.survey.springboot.controller.dto.*;
import com.survey.springboot.entity.Options;
import com.survey.springboot.entity.Paper;
import com.survey.springboot.entity.Question;
import com.survey.springboot.mapper.AnswerMapper;
import com.survey.springboot.mapper.OptionsMapper;
import com.survey.springboot.mapper.PaperMapper;
import com.survey.springboot.mapper.QuestionMapper;
import com.survey.springboot.service.IPaperService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-03
 */
@Service
public class PaperServiceImpl extends ServiceImpl<PaperMapper, Paper> implements IPaperService {

    private final static String EMPTY="无效ID";
    private final static String USER_EMPTY="用户未创建任何问卷";
    @Autowired
    PaperMapper paperMapper;
    @Autowired
    QuestionMapper questionMapper;
    @Autowired
    OptionsMapper optionsMapper;
    @Autowired
    AnswerMapper answerMapper;

    @Override   //添加问卷
    public ResponseDTO addPaper(PaperDTO paperdtO) {
        try {
            paperdtO.setUserId(he.userId);
            paperMapper.addPaper(paperdtO);
            return ResponseDTO.buildSuccess(paperdtO);
        }catch (Exception e){
            System.out.println(e);
            return ResponseDTO.buildFailure(e.getMessage());
        }
    }

    @Override   //更新问卷
    public ResponseDTO updatePaper(PaperDTO paperdtO) {
        try {
            PaperDTO paper=paperMapper.selectByPaperId(paperdtO.getId());
            if(paper==null)
                return ResponseDTO.buildFailure(EMPTY);
            else{
                paperMapper.updatePaper(paperdtO);
                return ResponseDTO.buildSuccess();
            }
        }catch (Exception e){
            System.out.println(e);
            return ResponseDTO.buildFailure(e.getMessage());
        }
    }
    
    @Scheduled(cron = "0 0/1 * * * ?")    //每分钟检查一次
    void checkPaperStatus(){
        List<PaperDTO> paperDtoList=paperMapper.getTimePapers();
        for(PaperDTO paperDtO:paperDtoList){
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDate = sdf.format(date);
            if(paperDtO.getStatus()==PaperStatus.INIT){    //改变问卷状态，开始
                if(currentDate.compareTo(paperDtO.getStartTime())>=0)
                    paperMapper.changeStatus(PaperStatus.START,paperDtO.getId());
            }
            else if(paperDtO.getStatus()==PaperStatus.START){   //改变用户状态，截止
                if(currentDate.compareTo(paperDtO.getEndTime())>=0)
                    paperMapper.changeStatus(PaperStatus.STOP,paperDtO.getId());
            }
        }
    }

    @Override   //删除问卷
    public ResponseDTO deletePaper(int paperId) {
        try {
            PaperDTO paper=paperMapper.selectByPaperId(paperId);
            if(paper==null)
                return ResponseDTO.buildFailure(EMPTY);
            else{
                paperMapper.deletePaper(paperId);
                List<Question> questionList=questionMapper.selectByPaperId(paperId);
                for(Question question:questionList)
                    optionsMapper.deleteByQuestionId(question.getId());
                questionMapper.deleteByPaperId(paperId);
                return ResponseDTO.buildSuccess();
            }
        }catch (Exception e){
            System.out.println(e);
            return ResponseDTO.buildFailure(e.getMessage());
        }
    }

    @Override   // 查看用户所有问卷
    public ResponseDTO getUserPapers(int userId) {
        try {
            List<PaperDTO> paperVOList=paperMapper.getUserPapers(userId);
            if(paperVOList==null)
                return ResponseDTO.buildFailure(USER_EMPTY);
            else
                return ResponseDTO.buildSuccess(paperVOList);
        }catch (Exception e){
            System.out.println(e);
            return ResponseDTO.buildFailure(e.getMessage());
        }
    }

    @Override   // 查看完整问卷
    public ResponseDTO checkPaper(int paperId) {
        try {
            PaperDTO paperDtO=paperMapper.selectByPaperId(paperId);
            if(paperDtO==null)
                return ResponseDTO.buildFailure(EMPTY);
            PaperDetail paperDetail=new PaperDetail();
            BeanUtils.copyProperties(paperDtO,paperDetail);

            List<QuestionDTO> questionVOList=new ArrayList<>();
            List<Question> questionList=questionMapper.selectByPaperId(paperId);
            for(Question question:questionList){
                int questionId=question.getId();
                List<OptionsDTO> optionsList=optionsMapper.selectByQuestionId(questionId);

                QuestionDTO questionVO=new QuestionDTO();
                BeanUtils.copyProperties(question,questionVO);
                questionVO.setOptions(optionsList);
                questionVOList.add(questionVO);
            }
            paperDetail.setQuestionList(questionVOList);
            return ResponseDTO.buildSuccess(paperDetail);

        }catch (Exception e){
            System.out.println(e);
            return ResponseDTO.buildFailure(e.getMessage());
        }
    }

    @Override   // 查看问卷统计信息
    public ResponseDTO reviewPaper(int paperId) {
        try {
            PaperDTO paperDtO=paperMapper.selectByPaperId(paperId);
            if(paperDtO==null)
                return ResponseDTO.buildFailure(EMPTY);
            else{
                PaperStatistic paperStatistic=new PaperStatistic();
                BeanUtils.copyProperties(paperDtO,paperStatistic);//将两个字段相同的对象进行属性值的复制

                List<QuestionStatistic> questionStatisticList=new ArrayList<>();
                List<Question> questionList=questionMapper.selectByPaperId(paperId);

                for(Question question:questionList){

                    QuestionStatistic questionStatistic=new QuestionStatistic();
                    BeanUtils.copyProperties(question,questionStatistic);

                    int questionId=question.getId();

                    if(question.getType()!=3){    //单选题和多选题
                        List<OptionsDTO> optionsList=optionsMapper.selectByQuestionId(questionId);
                        List<OptionStatistic> optionStatisticList =new ArrayList<>();
                        for(OptionsDTO options:optionsList){   //先都转成另一个DTO
                            OptionStatistic optionStatistic =new OptionStatistic();
                            BeanUtils.copyProperties(options, optionStatistic);
                            optionStatistic.setSelectedNum(0);   //后面用于+1
                            optionStatisticList.add(optionStatistic);
                        }

                        List<AnswerDTO> answerVOList=answerMapper.selectByQuestionId(questionId);

                        for(AnswerDTO answerVO:answerVOList){
                            String answerContent=answerVO.getAnswerContent();
                            String[] optionSequenceList=answerContent.split(",");
                            for(String sequenceStr:optionSequenceList){
                                int sequence=Integer.valueOf(sequenceStr);
                                for(OptionStatistic optionStatistic : optionStatisticList){
                                    if(optionStatistic.getSequence()==sequence){
                                        optionStatistic.setSelectedNum(optionStatistic.getSelectedNum()+1);
                                        break;
                                    }
                                }
                            }
                        }
                        questionStatistic.setOptionStatistics(optionStatisticList);
                    }
                    else{    //简答题
                        questionStatistic.setAnswerVOList(answerMapper.selectByQuestionId(questionId));
                    }
                    questionStatistic.setFilledInNum(answerMapper.selectByQuestionId(questionId).size());   //此题填写人数
                    questionStatisticList.add(questionStatistic);
                }
                paperStatistic.setQuestionStatistics(questionStatisticList);
                return ResponseDTO.buildSuccess(paperStatistic);
            }
        }catch (Exception e){
            System.out.println(e);
            return ResponseDTO.buildFailure(e.getMessage());
        }
    }
}
