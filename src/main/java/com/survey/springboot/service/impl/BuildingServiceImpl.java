package com.survey.springboot.service.impl;

import com.survey.springboot.entity.Building;
import com.survey.springboot.mapper.BuildingMapper;
import com.survey.springboot.service.IBuildingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-02
 */
@Service
public class BuildingServiceImpl extends ServiceImpl<BuildingMapper, Building> implements IBuildingService {

}
