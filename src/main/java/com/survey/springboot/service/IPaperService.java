package com.survey.springboot.service;

import com.survey.springboot.controller.dto.PaperDTO;
import com.survey.springboot.controller.dto.ResponseDTO;
import com.survey.springboot.entity.Paper;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-03
 */


public interface IPaperService extends IService<Paper> {
    ResponseDTO addPaper(PaperDTO paperVO); //添加问卷

    ResponseDTO updatePaper(PaperDTO paperVO); //更新问卷

    ResponseDTO deletePaper(int paperId); //删除问卷

    ResponseDTO getUserPapers(int userId); /// 查看用户所有问卷

    ResponseDTO checkPaper(int paperId); // 查看完整问卷

    ResponseDTO reviewPaper(int paperId); // 查看问卷统计信息
}
