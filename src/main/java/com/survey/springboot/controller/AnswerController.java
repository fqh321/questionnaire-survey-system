package com.survey.springboot.controller;


import com.survey.springboot.controller.dto.AnswerDTO;
import com.survey.springboot.controller.dto.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

import com.survey.springboot.service.IAnswerService;
import com.survey.springboot.entity.Answer;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-02
 */
@RestController
@RequestMapping("/api/answer")
public class AnswerController {
    @Autowired
    IAnswerService answerService;

    @PostMapping("/addAnswers")
    public ResponseDTO addAnswers(@RequestBody List<AnswerDTO> answerVOList){
        return answerService.addAnswers(answerVOList);
    }

    @GetMapping("/{paperId}/reviewAnswers")
    public ResponseDTO reviewAnswers(@PathVariable Integer paperId){
        return answerService.reviewAnswers(paperId);
    }
}

