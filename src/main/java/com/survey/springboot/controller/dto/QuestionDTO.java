package com.survey.springboot.controller.dto;

import com.survey.springboot.entity.Options;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDTO {
    private Integer id;
    private Integer paperId;
    private Integer type;
    private String title;
    private List<OptionsDTO> options;
}
