package com.survey.springboot.controller.dto;

import lombok.Data;

@Data
public class ResponseDTO {
    private Boolean success;    //调用是否成功

    private String message;     //返回的提示信息

    private Object content;     //内容

    public static ResponseDTO buildSuccess(){
        ResponseDTO response=new ResponseDTO();
        response.setSuccess(true);
        return response;
    }

    public static ResponseDTO buildSuccess(Object content){
        ResponseDTO response=new ResponseDTO();
        response.setContent(content);
        response.setSuccess(true);
        return response;
    }

    public static ResponseDTO buildFailure(String message){
        ResponseDTO response=new ResponseDTO();
        response.setSuccess(false);
        response.setMessage(message);
        System.out.println(message);
        return response;
    }
}
