package com.survey.springboot.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserVO {
    private Integer id;
    private String name;
    private String password;
}
