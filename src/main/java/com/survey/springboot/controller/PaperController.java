package com.survey.springboot.controller;


import com.survey.springboot.controller.dto.PaperDTO;
import com.survey.springboot.controller.dto.ResponseDTO;
import com.survey.springboot.controller.dto.UserDTO;
import com.survey.springboot.controller.dto.he;
import com.survey.springboot.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

import com.survey.springboot.service.IPaperService;
import com.survey.springboot.entity.Paper;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-03
 */
@RestController
@RequestMapping("/api/paper")
public class PaperController {
    @Autowired
    IPaperService paperService;
    @Autowired
    IUserService userService;
    public int userId2=0;
    @PostMapping("/addPaper")
    public ResponseDTO addPaper(@RequestBody PaperDTO paperVO){
        return paperService.addPaper(paperVO);
    }

    @PostMapping("/updatePaper")
    public ResponseDTO updatePaper(@RequestBody PaperDTO paperVO){
        return paperService.updatePaper(paperVO);
    }

    @GetMapping("/{paperId}/deletePaper")
    public ResponseDTO invalidatePaper(@PathVariable Integer paperId){
        return paperService.deletePaper(paperId);
    }

//    @GetMapping("/getUserPapers/{userId}")
//    public ResponseDTO getUserPapers(@PathVariable Integer userId){
//        return paperService.getUserPapers(userId);
//    }
    @GetMapping("/getUserPapers/{userId}")
    public ResponseDTO getUserPapers(@PathVariable Integer userId){
        return paperService.getUserPapers(he.userId);
    }
    @GetMapping("/{paperId}/checkPaper")
    public ResponseDTO checkPaper(@PathVariable Integer paperId){
        return paperService.checkPaper(paperId);
    }

    @GetMapping("/{paperId}/reviewPaper")
    public ResponseDTO reviewPaper(@PathVariable Integer paperId){
        return paperService.reviewPaper(paperId);
    }
}
