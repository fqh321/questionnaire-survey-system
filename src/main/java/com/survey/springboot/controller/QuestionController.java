package com.survey.springboot.controller;


import com.survey.springboot.controller.dto.QuestionDTO;
import com.survey.springboot.controller.dto.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

import com.survey.springboot.service.IQuestionService;
import com.survey.springboot.entity.Question;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-03
 */
@RestController
@RequestMapping("/api/question")
public class QuestionController {

    @Autowired
    IQuestionService questionService;

    @GetMapping("/{paperId}/addQuestion")
    public ResponseDTO addQuestion(@PathVariable Integer paperId){
        return questionService.addQuestion(paperId);
    }

    @PostMapping("/updateQuestion")
    public ResponseDTO updateQuestion(@RequestBody QuestionDTO questionVO){
        return questionService.updateQuestion(questionVO);
    }

    @GetMapping("/{questionId}/deleteQuestion")
    public ResponseDTO deleteQuestion(@PathVariable Integer questionId){
        return questionService.deleteQuestion(questionId);
    }
}
