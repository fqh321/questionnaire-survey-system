package com.survey.springboot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-03
 */
@Getter
@Setter
  @ApiModel(value = "Paper对象", description = "")
public class Paper implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("问卷ID")
        @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      @ApiModelProperty("用户ID")
      private Integer userId;

      @ApiModelProperty("题目")
      private String title;

      @ApiModelProperty("描述")
      private String description;

      @ApiModelProperty("开始时间")
      private LocalDateTime startTime;

      @ApiModelProperty("结束时间")
      private LocalDateTime endTime;

      @ApiModelProperty("状态")
      private String status;


}
