package com.survey.springboot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-02
 */
@Getter
@Setter
  @ApiModel(value = "Answer对象", description = "")
public class Answer implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("答案ID")
        @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      @ApiModelProperty("问题ID")
      private Integer questionId;

      @ApiModelProperty("问卷ID")
      private Integer paperId;

      @ApiModelProperty("问题类型")
      private Integer questionType;

      @ApiModelProperty("创建时间")
      private LocalDateTime createTime;

      @ApiModelProperty("回答状态")
      private String answerContent;

      @ApiModelProperty("用户ID")
      private String userUuid;


}
