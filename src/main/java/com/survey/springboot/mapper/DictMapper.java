package com.survey.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.survey.springboot.entity.Dict;

public interface DictMapper extends BaseMapper<Dict> {
}
