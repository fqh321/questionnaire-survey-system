package com.survey.springboot.mapper;

import com.survey.springboot.common.PaperStatus;
import com.survey.springboot.controller.dto.PaperDTO;
import com.survey.springboot.entity.Paper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-03
 */
public interface PaperMapper extends BaseMapper<Paper> {
    int addPaper(PaperDTO paperVO);

    void updatePaper(PaperDTO paperVO);

    PaperDTO selectByPaperId(int paperId);

    void deletePaper(int paperId);

    List<PaperDTO> getUserPapers(int userId);

    List<PaperDTO> getTimePapers();

    void changeStatus(@Param("paperStatus") PaperStatus paperStatus, @Param("id") int id);
}
