package com.survey.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.survey.springboot.controller.dto.OptionsDTO;
import com.survey.springboot.entity.Options;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-03
 */
public interface OptionsMapper extends BaseMapper<Options> {
    int addOption(OptionsDTO optionsVO);    //增加选项

    OptionsDTO findOption(OptionsDTO optionsVO);    //

    void updateOption(OptionsDTO optionsVO);    //更新选项

    void deleteByQuestionId(Integer questionId);    //根据问题ID删除选项

    List<OptionsDTO> selectByQuestionId(int questionId);    //通过问题ID查找选项
}
