package com.survey.springboot.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.survey.springboot.controller.dto.UserDTO;
import com.survey.springboot.controller.dto.UserPasswordDTO;
import com.survey.springboot.controller.dto.UserVO;
import com.survey.springboot.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-02
 */
public interface UserMapper extends BaseMapper<User> {

    @Update("update sys_user set password = #{newPassword} where username = #{username} and password = #{password}")
    int updatePassword(UserPasswordDTO userPasswordDTO);

    Page<User> findPage(Page<User> page, @Param("username") String username, @Param("email") String email, @Param("address") String address);

    int addUser(UserVO userVO);

    UserVO getUserByName(String name);
}
