package com.survey.springboot.mapper;

import com.survey.springboot.controller.dto.AnswerDTO;
import com.survey.springboot.entity.Answer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-02
 */
public interface AnswerMapper extends BaseMapper<Answer> {
    int addAnswer(AnswerDTO answerVO);

    List<AnswerDTO> selectByQuestionId(int questionId);

    List<String> selectUUIDbyPaper(int paperId);

    List<AnswerDTO> selectAnswersByUUID(String UUID);

    String selectOption(@Param("question_id")int question_id, @Param("sequence")String sequence);
}
