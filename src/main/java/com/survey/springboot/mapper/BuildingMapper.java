package com.survey.springboot.mapper;

import com.survey.springboot.entity.Building;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-02
 */
public interface BuildingMapper extends BaseMapper<Building> {

}
