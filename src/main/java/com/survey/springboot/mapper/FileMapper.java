package com.survey.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.survey.springboot.entity.Files;

public interface FileMapper extends BaseMapper<Files> {
}
