package com.survey.springboot.mapper;

import com.survey.springboot.entity.Question;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 樊清华
 * @since 2023-06-03
 */
public interface QuestionMapper extends BaseMapper<Question> {
    int addQuestion(Question question); //增加问题

    void updateQuestion(Question question); //更新问题

    Question selectByQuestionId(int id);    //通过问题ID查找

    void deleteQuestion(int questionId);    //删除问题

    void deleteByPaperId(int paperId);  //通过问卷ID删除问题

    List<Question> selectByPaperId(int paperId);    //通过问卷ID查找
}
