/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : survey3

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 29/08/2023 10:39:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for answer
-- ----------------------------
DROP TABLE IF EXISTS `answer`;
CREATE TABLE `answer`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '答案ID',
  `question_id` int NOT NULL COMMENT '问题ID',
  `paper_id` int NOT NULL COMMENT '问卷ID',
  `question_type` int NOT NULL COMMENT '问题类型',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `answer_content` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '回答状态',
  `user_uuid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of answer
-- ----------------------------
INSERT INTO `answer` VALUES (1, 4, 2, 1, '1000-01-01 00:00:00', '2', '1');
INSERT INTO `answer` VALUES (2, 5, 2, 2, '1000-01-01 00:00:00', '2,3', '1');
INSERT INTO `answer` VALUES (3, 6, 2, 3, '1000-01-01 00:00:00', '测试问卷667-3：foofoofoofoo', '1');
INSERT INTO `answer` VALUES (4, 4, 2, 1, '1000-01-01 00:00:00', '2', '2');
INSERT INTO `answer` VALUES (5, 5, 2, 2, '1000-01-01 00:00:00', '1,3', '2');
INSERT INTO `answer` VALUES (6, 6, 2, 3, '1000-01-01 00:00:00', '测试问卷667-3：bazbazbaz', '2');
INSERT INTO `answer` VALUES (7, 4, 2, 1, '1000-01-01 00:00:00', '1', '3');
INSERT INTO `answer` VALUES (8, 5, 2, 2, '1000-01-01 00:00:00', '1,2,3,4', '3');
INSERT INTO `answer` VALUES (9, 6, 2, 3, '1000-01-01 00:00:00', '测试问卷667-3：barbarabr', '3');
INSERT INTO `answer` VALUES (10, 4, 2, 1, '1000-01-01 00:00:00', '2', '4');
INSERT INTO `answer` VALUES (11, 5, 2, 2, '1000-01-01 00:00:00', '4', '4');
INSERT INTO `answer` VALUES (12, 6, 2, 3, '1000-01-01 00:00:00', '测试问卷667-3：hhhhhhhhh', '4');
INSERT INTO `answer` VALUES (25, 7, 3, 1, '2023-06-05 16:43:38', '1', 'bc77ba4f637a4fff91463cf6e456abda');
INSERT INTO `answer` VALUES (26, 8, 3, 2, '2023-06-05 16:43:38', '1,5,4', 'bc77ba4f637a4fff91463cf6e456abda');
INSERT INTO `answer` VALUES (27, 9, 3, 3, '2023-06-05 16:43:38', '123', 'bc77ba4f637a4fff91463cf6e456abda');
INSERT INTO `answer` VALUES (28, 4, 2, 1, '2023-06-05 18:47:21', '2', 'b4c460e65e3e4e06b0f7c70c2f83c7d2');
INSERT INTO `answer` VALUES (29, 5, 2, 2, '2023-06-05 18:47:21', '4', 'b4c460e65e3e4e06b0f7c70c2f83c7d2');
INSERT INTO `answer` VALUES (30, 6, 2, 3, '2023-06-05 18:47:21', 'rr', 'b4c460e65e3e4e06b0f7c70c2f83c7d2');
INSERT INTO `answer` VALUES (31, 4, 2, 1, '2023-06-06 10:25:23', '2', '8df0faee8b5b455ca277661bd14b7706');
INSERT INTO `answer` VALUES (32, 5, 2, 2, '2023-06-06 10:25:23', '4', '8df0faee8b5b455ca277661bd14b7706');
INSERT INTO `answer` VALUES (33, 6, 2, 3, '2023-06-06 10:25:23', '8799', '8df0faee8b5b455ca277661bd14b7706');
INSERT INTO `answer` VALUES (34, 18, 10, 1, '2023-06-06 15:23:20', '1', '6d85eaa5af264e58a2208dc597471f57');
INSERT INTO `answer` VALUES (35, 19, 10, 3, '2023-06-06 15:23:20', '14y', '6d85eaa5af264e58a2208dc597471f57');
INSERT INTO `answer` VALUES (36, 20, 12, 1, '2023-06-07 14:24:34', '1', 'd327f3b61248419bb92014c0980cdb80');
INSERT INTO `answer` VALUES (37, 21, 13, 1, '2023-06-07 23:44:11', '1', '5bbad38a737e413d9091bc732d443683');
INSERT INTO `answer` VALUES (38, 21, 13, 1, '2023-06-07 23:44:14', '2', 'fc3bd72464b344cdb8fb14fda53f7c41');
INSERT INTO `answer` VALUES (39, 21, 13, 1, '2023-06-08 14:10:24', '1', 'e1fcd3ecb9f749ceb2471997c996d178');
INSERT INTO `answer` VALUES (40, 23, 11, 3, '2023-06-08 14:14:39', 'csa', '16595a9e5ebf4a88967ca52c12770e10');
INSERT INTO `answer` VALUES (41, 24, 11, 2, '2023-06-08 14:14:39', '1,2,3', '16595a9e5ebf4a88967ca52c12770e10');
INSERT INTO `answer` VALUES (42, 4, 2, 1, '2023-06-08 15:22:29', '2', 'f01b99afec3f4e128a72a02772cf5ae8');
INSERT INTO `answer` VALUES (43, 5, 2, 2, '2023-06-08 15:22:29', '1,3', 'f01b99afec3f4e128a72a02772cf5ae8');
INSERT INTO `answer` VALUES (44, 6, 2, 3, '2023-06-08 15:22:29', '134', 'f01b99afec3f4e128a72a02772cf5ae8');
INSERT INTO `answer` VALUES (45, 4, 2, 1, '2023-06-12 14:11:23', '1', 'ea233934b4c04336b64e4b088c6caa7f');
INSERT INTO `answer` VALUES (46, 5, 2, 2, '2023-06-12 14:11:23', '1,2', 'ea233934b4c04336b64e4b088c6caa7f');
INSERT INTO `answer` VALUES (47, 6, 2, 3, '2023-06-12 14:11:23', 'wq', 'ea233934b4c04336b64e4b088c6caa7f');
INSERT INTO `answer` VALUES (48, 30, 18, 3, '2023-06-12 15:36:57', '胡守成', '9b6faa6383574dbbaf55a3c7c6a5ce53');
INSERT INTO `answer` VALUES (49, 31, 18, 3, '2023-06-12 15:36:57', '058', '9b6faa6383574dbbaf55a3c7c6a5ce53');
INSERT INTO `answer` VALUES (50, 32, 18, 1, '2023-06-12 15:36:57', '2', '9b6faa6383574dbbaf55a3c7c6a5ce53');
INSERT INTO `answer` VALUES (51, 33, 18, 3, '2023-06-12 15:36:57', '22', '9b6faa6383574dbbaf55a3c7c6a5ce53');
INSERT INTO `answer` VALUES (52, 34, 18, 1, '2023-06-12 15:36:57', '2', '9b6faa6383574dbbaf55a3c7c6a5ce53');
INSERT INTO `answer` VALUES (53, 35, 18, 1, '2023-06-12 15:36:57', '2', '9b6faa6383574dbbaf55a3c7c6a5ce53');
INSERT INTO `answer` VALUES (54, 36, 18, 2, '2023-06-12 15:36:57', '1,2,3,4', '9b6faa6383574dbbaf55a3c7c6a5ce53');
INSERT INTO `answer` VALUES (55, 37, 18, 3, '2023-06-12 15:36:57', '好人', '9b6faa6383574dbbaf55a3c7c6a5ce53');
INSERT INTO `answer` VALUES (56, 21, 13, 1, '2023-06-13 19:50:25', '1', 'd67ffa417681446f86523acc289dff83');
INSERT INTO `answer` VALUES (57, 40, 22, 1, '2023-06-14 17:31:07', '1', 'a915689c85bb4050a0358a1809f73a23');
INSERT INTO `answer` VALUES (58, 4, 2, 1, '2023-06-14 17:31:55', '1', '34662a2ca7f141ba9ba0ff164823bd7e');
INSERT INTO `answer` VALUES (59, 5, 2, 2, '2023-06-14 17:31:55', '1,2', '34662a2ca7f141ba9ba0ff164823bd7e');
INSERT INTO `answer` VALUES (60, 6, 2, 3, '2023-06-14 17:31:55', 'i', '34662a2ca7f141ba9ba0ff164823bd7e');
INSERT INTO `answer` VALUES (61, 41, 23, 1, '2023-06-18 16:45:47', '1', 'ca292a5d68f943b787de2f0f951e4066');
INSERT INTO `answer` VALUES (62, 42, 23, 1, '2023-06-18 16:45:47', '1', 'ca292a5d68f943b787de2f0f951e4066');
INSERT INTO `answer` VALUES (63, 43, 23, 2, '2023-06-18 16:45:47', '2,1,3', 'ca292a5d68f943b787de2f0f951e4066');
INSERT INTO `answer` VALUES (64, 44, 23, 3, '2023-06-18 16:45:47', '满意', 'ca292a5d68f943b787de2f0f951e4066');
INSERT INTO `answer` VALUES (65, 47, 26, 1, '2023-06-18 20:10:10', '3', '1a758ea83f2a49dc956b771f6a9c0957');
INSERT INTO `answer` VALUES (66, 48, 26, 2, '2023-06-18 20:10:10', '1,2,3', '1a758ea83f2a49dc956b771f6a9c0957');
INSERT INTO `answer` VALUES (67, 49, 26, 3, '2023-06-18 20:10:10', '1245r1q23r', '1a758ea83f2a49dc956b771f6a9c0957');
INSERT INTO `answer` VALUES (68, 50, 27, 1, '2023-06-18 20:30:15', '3', '5bc0801584e44033b03182add5bf6c1d');
INSERT INTO `answer` VALUES (69, 51, 27, 2, '2023-06-18 20:30:15', '2,1,3', '5bc0801584e44033b03182add5bf6c1d');
INSERT INTO `answer` VALUES (70, 52, 27, 3, '2023-06-18 20:30:15', '12', '5bc0801584e44033b03182add5bf6c1d');

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '内容',
  `user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '发布人',
  `time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES (4, '湖北中医药大学2023年普通专升本招生简章', '[自己看，本人懒得写](https://jwc.hbtcm.edu.cn/info/1043/2838.htm)', '管理员', '2023-03-19 23:04:44');
INSERT INTO `article` VALUES (5, '关于开展2021届毕业生培养质量评价的通知', '为全面了解本校2021届毕业生的就业、创业现状及发展等方面情况，促进本校人才培养质量的提升和就业创业服务的改进，学校将于近期开展2021届毕业生培养质量评价。诚邀毕业生积极参与！\n\n评价时间：2022年9月底-2022年10月底\n\n评价对象：2021届毕业生\n\n评价方式：问卷采用在线答题方式进行，答题入口将通过邮件或短信发送给各位毕业生。\n\n郑重承诺：本问卷调研将对毕业生个人信息严格保密，问卷所收集到的相关信息仅作分析研究使用。请广大毕业生积极配合，如实填写。\n\n再次感谢您的信任和支持！\n\n', '管理员', '2023-03-19 23:09:46');
INSERT INTO `article` VALUES (6, '2022级普通本科生校内转专业考试通过学生名单公示', '根据《关于印发<湖北中医药大学普通全日制本科生校内转专业实施办法>（试行）的通知》（中医校〔2021〕88号）文件精神，现将2022级普通本科生参加校内转专业考试通过名单予以公示，公示时间为2023年3月17日至2023年3月20日。\n\n如有疑义，请3月20日前与教务处学籍科联系，联系电话68890049。\n\n\n\n\n\n\n\n                                           教务处\n\n                                       2023年3月17日\n\n\n\n2022级普通本科生校内转专业考试通过学生名单\n\n姓名\n\n学号\n\n潘韵能\n\n2022301010690\n\n朱洪瑶\n\n2022301010671\n\n钟鑫灿\n\n2022302012336\n\n何姝婧\n\n2022302012331\n\n白嘉捷\n\n2022302011377\n\n段宇衡\n\n2022303010549\n\n张晓晖\n\n2022303010458\n\n朱文龙\n\n2022303010469\n\n余乐\n\n2022303010478\n\n白银银\n\n2022303012381\n\n田纪宸\n\n2021303013424\n\n李萌驰\n\n2022303012370\n\n柴云富\n\n2022303012434\n\n程坤\n\n2022303012487\n\n卓容儿\n\n2022303013377\n\n贾建宇\n\n2022303010320\n\n徐宇然\n\n2022313010221\n\n王栀祺\n\n2022308011399\n\n姜子珍\n\n2022304012532\n\n张晟杰\n\n2022305013538\n\n肖湘怡\n\n2022306012903\n\n叶扬慧\n\n2022306012878\n\n胡梦\n\n2022306012962\n\n姜文婧\n\n2022306013751\n\n袁雅丹\n\n2022306013801\n\n欧阳高杰\n\n2022303012384\n\n刘洁瑞\n\n2022303012386\n\n卢小朵\n\n2022303012490\n\n李莎\n\n2022303013441\n\n邱迪\n\n2022303010295\n\n邵贝贝\n\n2022309011259\n\n高浩怡\n\n2022313010208\n\n赵金慧\n\n2022313010198\n\n石璨菲\n\n2022313010258\n\n唐畅\n\n2022308011990\n\n徐芯嫒\n\n2022308010099\n\n周正兰\n\n2022308010162\n\n李诗怡\n\n2022308010147\n\n康昊文\n\n2022308011416\n\n姚军航\n\n2022308013048\n\n吴忻妮\n\n2022305012731\n\n岳欣悦\n\n2022305012734\n\n邹欣\n\n2022305013551\n\n余爽\n\n2022306010034\n\n文仕祺\n\n2022306011456\n\n\n', '管理员', '2023-03-19 23:10:22');
INSERT INTO `article` VALUES (8, '个人日志5-29', '组织组员一起进行需求分析，在网上查找资料，了解问卷调查系统的相关流程，搞清用户需求。', '管理员', '2023-06-06 16:38:28');
INSERT INTO `article` VALUES (9, '个人日志5-30', '整合需求分析，写需求分析说明书，画用例图,编写文档。', '管理员', '2023-06-06 16:40:05');
INSERT INTO `article` VALUES (10, '个人日志5-31', '画用例图，写需求文档，进行模块设计，数据库设计，考虑使用非关系型数据库MongDB还是关系型数据库mysql,考虑到对于mysql数据库更为熟悉，最终使用关系型数据库mysql。\n', '管理员', '2023-06-06 16:41:17');
INSERT INTO `article` VALUES (11, '个人日志6-2', '搭建后台环境，打算使用springboot+mybatis+mybatis-plus搭建后台。首先选择java版本，mysql版本，lombok等，在pom.xml文件中配置各种依赖。', '管理员', '2023-06-06 16:42:18');
INSERT INTO `article` VALUES (12, '个人日志6-5', '接口设计，搭建后台框架，设计类', '管理员', '2023-06-06 16:42:49');
INSERT INTO `article` VALUES (13, '个人日志6-6', '搭建前端框架', '管理员', '2023-06-06 16:43:34');
INSERT INTO `article` VALUES (14, '个人日志6-7', '解决如何通过用户登录后的id查询数据库绑定对应的问卷id,再通过问卷id查找对应的单选题，多选题,解答题id并与答案相对应，在请求路径上将问卷id加上，一级一级的往下找问题id和答案id，后来发现通过登录获取用户id然后找到问卷id，题目，答案id太麻烦，传递的参数不能到达后台，导致400和500错误。	', '管理员', '2023-06-09 16:55:58');
INSERT INTO `article` VALUES (15, '个人日志6-8', '关于用户id查找问卷id然后查询答案和问题id过于复杂，查找资料尚不能解决，后来吃饭时突然想到可以将各种id值直接后台传后台，刚开始尝试准备在controller层在路径处进行传值，但是不规范，取的值也是null，然后改变思路，在service层进行id的获取和传参，我首先建立了一个类里有一个静态变量id用来存储用户id，在service层的login方法处通过登录的用户名获取用户id，然后复制给静态变量id,这样其他controller层和service层就可以直接获取到用户id，不用经过前端传值，更加简单方便', '管理员', '2023-06-09 17:05:39');
INSERT INTO `article` VALUES (16, '个人日志6-10', '商务', '管理员', '2023-06-09 17:06:25');
INSERT INTO `article` VALUES (17, '个人日志6-12', '今天进行前端管理员界面首页通过导入echarts实现数据可视化产生饼图和柱状图，并与后台数据库相关联动态数据变化', '管理员', '2023-06-12 17:09:12');

-- ----------------------------
-- Table structure for building
-- ----------------------------
DROP TABLE IF EXISTS `building`;
CREATE TABLE `building`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '楼栋',
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '单元',
  `pid` int NULL DEFAULT NULL COMMENT '父级id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of building
-- ----------------------------
INSERT INTO `building` VALUES (1, '1栋', '1栋', NULL);
INSERT INTO `building` VALUES (2, '2栋', '2栋', NULL);
INSERT INTO `building` VALUES (3, '1单元', '1单元', 1);
INSERT INTO `building` VALUES (4, '2单元', '2单元', 1);
INSERT INTO `building` VALUES (5, '1单元', '1单元', 2);
INSERT INTO `building` VALUES (6, '2单元', '2单元', 2);
INSERT INTO `building` VALUES (7, '101', '101', 3);
INSERT INTO `building` VALUES (8, '201', '201', 3);
INSERT INTO `building` VALUES (9, '101', '101', 4);
INSERT INTO `building` VALUES (10, '201', '201', 4);

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '课程名称',
  `score` int NULL DEFAULT NULL COMMENT '学分',
  `times` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '上课时间',
  `state` tinyint(1) NULL DEFAULT NULL COMMENT '是否开课',
  `teacher_id` int NULL DEFAULT NULL COMMENT '授课老师id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES (1, '大学物理', 10, '40', 0, 17);
INSERT INTO `course` VALUES (2, '高等数学', 10, '45', NULL, 16);
INSERT INTO `course` VALUES (3, '大学英语', 10, '30', NULL, 16);

-- ----------------------------
-- Table structure for options
-- ----------------------------
DROP TABLE IF EXISTS `options`;
CREATE TABLE `options`  (
  `question_id` int NOT NULL COMMENT '问题ID',
  `sequence` int NOT NULL COMMENT '序列',
  `content` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '选项',
  PRIMARY KEY (`question_id`, `sequence`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of options
-- ----------------------------
INSERT INTO `options` VALUES (4, 1, '667-1-选项一');
INSERT INTO `options` VALUES (4, 2, '667-1-选项二');
INSERT INTO `options` VALUES (5, 1, '667-2-选项一');
INSERT INTO `options` VALUES (5, 2, '667-2-选项二');
INSERT INTO `options` VALUES (5, 3, '667-2-选项三');
INSERT INTO `options` VALUES (5, 4, '667-2-选项四');
INSERT INTO `options` VALUES (7, 1, 'sag');
INSERT INTO `options` VALUES (7, 2, 'dzfjh');
INSERT INTO `options` VALUES (8, 1, 'szh');
INSERT INTO `options` VALUES (8, 2, 'sh');
INSERT INTO `options` VALUES (8, 3, 'hsZ');
INSERT INTO `options` VALUES (8, 4, 'hzed');
INSERT INTO `options` VALUES (8, 5, 'hzedsd');
INSERT INTO `options` VALUES (11, 1, 'tt');
INSERT INTO `options` VALUES (11, 2, 'tt');
INSERT INTO `options` VALUES (11, 3, 'tt');
INSERT INTO `options` VALUES (11, 4, 't');
INSERT INTO `options` VALUES (12, 1, '123');
INSERT INTO `options` VALUES (12, 2, '123');
INSERT INTO `options` VALUES (14, 1, '123');
INSERT INTO `options` VALUES (14, 2, '123');
INSERT INTO `options` VALUES (14, 3, '123');
INSERT INTO `options` VALUES (15, 1, '37');
INSERT INTO `options` VALUES (15, 2, '37');
INSERT INTO `options` VALUES (15, 3, '37');
INSERT INTO `options` VALUES (15, 4, '37');
INSERT INTO `options` VALUES (17, 1, 'hterws');
INSERT INTO `options` VALUES (17, 2, 'serwtjh');
INSERT INTO `options` VALUES (17, 3, 'a');
INSERT INTO `options` VALUES (17, 4, 'aeujh');
INSERT INTO `options` VALUES (18, 1, '1t');
INSERT INTO `options` VALUES (18, 2, '1t');
INSERT INTO `options` VALUES (20, 1, 'qw');
INSERT INTO `options` VALUES (20, 2, 'qwf');
INSERT INTO `options` VALUES (20, 3, 'qg');
INSERT INTO `options` VALUES (21, 1, 'eee');
INSERT INTO `options` VALUES (21, 2, 'eee');
INSERT INTO `options` VALUES (25, 1, '12');
INSERT INTO `options` VALUES (25, 2, '12');
INSERT INTO `options` VALUES (25, 3, '12');
INSERT INTO `options` VALUES (25, 4, '12');
INSERT INTO `options` VALUES (32, 1, '男');
INSERT INTO `options` VALUES (32, 2, '女');
INSERT INTO `options` VALUES (34, 1, '加入他们');
INSERT INTO `options` VALUES (34, 2, '吃完饭就走');
INSERT INTO `options` VALUES (34, 3, '熟视无睹');
INSERT INTO `options` VALUES (34, 4, '举报老师');
INSERT INTO `options` VALUES (35, 1, '支持');
INSERT INTO `options` VALUES (35, 2, '一般');
INSERT INTO `options` VALUES (35, 3, '反对');
INSERT INTO `options` VALUES (35, 4, '极其反对');
INSERT INTO `options` VALUES (36, 1, '乐观');
INSERT INTO `options` VALUES (36, 2, '沉稳');
INSERT INTO `options` VALUES (36, 3, '大方');
INSERT INTO `options` VALUES (36, 4, '外表好');
INSERT INTO `options` VALUES (38, 1, '1000');
INSERT INTO `options` VALUES (38, 2, '2000');
INSERT INTO `options` VALUES (40, 1, '12');
INSERT INTO `options` VALUES (40, 2, '12');
INSERT INTO `options` VALUES (41, 1, '是');
INSERT INTO `options` VALUES (41, 2, '否');
INSERT INTO `options` VALUES (42, 1, '1000');
INSERT INTO `options` VALUES (42, 2, '2000');
INSERT INTO `options` VALUES (43, 1, '吃饭');
INSERT INTO `options` VALUES (43, 2, '逛街');
INSERT INTO `options` VALUES (43, 3, '娱乐');
INSERT INTO `options` VALUES (43, 4, '学习');
INSERT INTO `options` VALUES (50, 1, '12');
INSERT INTO `options` VALUES (50, 2, '21');
INSERT INTO `options` VALUES (50, 3, '12');
INSERT INTO `options` VALUES (51, 1, '21');
INSERT INTO `options` VALUES (51, 2, '21');
INSERT INTO `options` VALUES (51, 3, '21');

-- ----------------------------
-- Table structure for paper
-- ----------------------------
DROP TABLE IF EXISTS `paper`;
CREATE TABLE `paper`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '问卷ID',
  `user_id` int NOT NULL COMMENT '用户ID',
  `title` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '题目',
  `description` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述',
  `start_time` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '结束时间',
  `status` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of paper
-- ----------------------------
INSERT INTO `paper` VALUES (2, 1, '测试问卷2（已发布）', '测试用问卷，拥有三个问题，已发布', '2023-06-09 15:08:56', NULL, 'START');
INSERT INTO `paper` VALUES (3, 1, 'fdgf', 'hfdd', '2023-06-21 15:00:41', NULL, 'START');
INSERT INTO `paper` VALUES (5, 1, '123', '123', '2023-03-01 15:00:48', NULL, 'INIT');
INSERT INTO `paper` VALUES (6, 1, '34', '34', '2023-11-17 15:00:54', NULL, 'INIT');
INSERT INTO `paper` VALUES (8, 1, 'hrewy', 'eawtju', '2023-06-27 15:01:05', NULL, 'START');
INSERT INTO `paper` VALUES (10, 1, '124', '125', '2025-10-12 15:01:12', NULL, 'START');
INSERT INTO `paper` VALUES (12, 2, '124', '1216', '2023-06-12 15:01:26', NULL, 'STOP');
INSERT INTO `paper` VALUES (13, 2, 'f', 'f2', '2023-12-12 15:01:29', NULL, 'START');
INSERT INTO `paper` VALUES (14, 2, '1223', '333', '2023-01-12 15:01:35', NULL, 'INIT');
INSERT INTO `paper` VALUES (18, 1, '大学生恋爱观调查问卷', '对于大学生在校期间是否愿意谈恋爱的问卷调查。', '2023-06-06 16:01:37', NULL, 'STOP');
INSERT INTO `paper` VALUES (19, 1, '7', '7', '2023-06-20 16:01:42', NULL, 'INIT');
INSERT INTO `paper` VALUES (20, 48, '大学生消费观', '', '2023-06-13 00:00:00', '2023-06-23 00:00:00', 'INIT');
INSERT INTO `paper` VALUES (21, 1, '12', '1212', '2023-06-07 15:59:59', NULL, 'INIT');
INSERT INTO `paper` VALUES (27, 1, '12', '12', '2023-06-14 20:33:08', NULL, 'START');

-- ----------------------------
-- Table structure for person
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person`  (
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `sex` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of person
-- ----------------------------
INSERT INTO `person` VALUES ('樊清华', '111', '男', '四川', '成都');
INSERT INTO `person` VALUES ('齐小天', '2000-01-1', '男', '江苏', '南京');

-- ----------------------------
-- Table structure for question
-- ----------------------------
DROP TABLE IF EXISTS `question`;
CREATE TABLE `question`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '问题ID',
  `paper_id` int NOT NULL COMMENT '问卷ID',
  `type` int NULL DEFAULT NULL COMMENT '问题类型',
  `title` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问题题目',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of question
-- ----------------------------
INSERT INTO `question` VALUES (4, 2, 1, '测试问卷667-1：单选题');
INSERT INTO `question` VALUES (5, 2, 2, '测试问卷667-2：多选题');
INSERT INTO `question` VALUES (6, 2, 3, '测试问卷667-3：填充题');
INSERT INTO `question` VALUES (7, 3, 1, 'hZDSh');
INSERT INTO `question` VALUES (8, 3, 2, 'sdhzh');
INSERT INTO `question` VALUES (9, 3, 3, 'jdzsj');
INSERT INTO `question` VALUES (11, 1, 2, 'nih');
INSERT INTO `question` VALUES (12, 4, 1, '123');
INSERT INTO `question` VALUES (13, 4, 3, '123');
INSERT INTO `question` VALUES (14, 5, 1, '35222333');
INSERT INTO `question` VALUES (15, 6, 1, '36');
INSERT INTO `question` VALUES (17, 8, 1, 'hesrhj');
INSERT INTO `question` VALUES (18, 10, 1, 't1');
INSERT INTO `question` VALUES (19, 10, 3, '1y1y');
INSERT INTO `question` VALUES (20, 12, 1, '2');
INSERT INTO `question` VALUES (21, 13, 1, 'ne');
INSERT INTO `question` VALUES (25, 15, 1, '6.8');
INSERT INTO `question` VALUES (30, 18, 3, '姓名');
INSERT INTO `question` VALUES (31, 18, 3, '学号');
INSERT INTO `question` VALUES (32, 18, 1, '性别');
INSERT INTO `question` VALUES (33, 18, 3, '年龄');
INSERT INTO `question` VALUES (34, 18, 1, '在食堂看见别人卿卿我我，你会怎么办');
INSERT INTO `question` VALUES (35, 18, 1, '家人是否支持你谈恋爱');
INSERT INTO `question` VALUES (36, 18, 2, '你会找一个怎样的对象');
INSERT INTO `question` VALUES (37, 18, 3, '你希望对象是一个怎样的人，为什么');
INSERT INTO `question` VALUES (38, 20, 1, '一个月多少money');
INSERT INTO `question` VALUES (39, 21, NULL, NULL);
INSERT INTO `question` VALUES (40, 22, 1, '12');
INSERT INTO `question` VALUES (41, 23, 1, '本人是独生子女吗');
INSERT INTO `question` VALUES (42, 23, 1, '一个生活费大概多少');
INSERT INTO `question` VALUES (43, 23, 2, '生活费的主要去处');
INSERT INTO `question` VALUES (44, 23, 3, '你对于目前的生活费满意吗，你的期望生活费是多少\n');
INSERT INTO `question` VALUES (45, 24, NULL, NULL);
INSERT INTO `question` VALUES (46, 25, NULL, NULL);
INSERT INTO `question` VALUES (50, 27, 1, '12');
INSERT INTO `question` VALUES (51, 27, 2, '12');
INSERT INTO `question` VALUES (52, 27, 3, '21');

-- ----------------------------
-- Table structure for student_course
-- ----------------------------
DROP TABLE IF EXISTS `student_course`;
CREATE TABLE `student_course`  (
  `student_id` int NOT NULL,
  `course_id` int NOT NULL,
  PRIMARY KEY (`student_id`, `course_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of student_course
-- ----------------------------
INSERT INTO `student_course` VALUES (1, 3);
INSERT INTO `student_course` VALUES (28, 1);
INSERT INTO `student_course` VALUES (28, 2);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '内容',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '类型'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('user', 'el-icon-user', 'icon');
INSERT INTO `sys_dict` VALUES ('house', 'el-icon-house', 'icon');
INSERT INTO `sys_dict` VALUES ('menu', 'el-icon-menu', 'icon');
INSERT INTO `sys_dict` VALUES ('s-custom', 'el-icon-s-custom', 'icon');
INSERT INTO `sys_dict` VALUES ('s-grid', 'el-icon-s-grid', 'icon');
INSERT INTO `sys_dict` VALUES ('document', 'el-icon-document', 'icon');
INSERT INTO `sys_dict` VALUES ('coffee', 'el-icon-coffee\r\n', 'icon');
INSERT INTO `sys_dict` VALUES ('s-marketing', 'el-icon-s-marketing', 'icon');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文件名称',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文件类型',
  `size` bigint NULL DEFAULT NULL COMMENT '文件大小(kb)',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '下载链接',
  `md5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文件md5',
  `is_delete` tinyint(1) NULL DEFAULT 0 COMMENT '是否删除',
  `enable` tinyint(1) NULL DEFAULT 1 COMMENT '是否禁用链接',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES (17, '649002da71c8473db39892b4a758f875.png', 'png', 634, 'http://localhost:9090/file/8d966b0e6cf84fe191a72a58b8293b23.png', 'e1a9407cd0e937c4b411a93b7aca7c87', 1, 0);
INSERT INTO `sys_file` VALUES (18, '1641024983532cf.png', 'png', 634, 'http://localhost:9090/file/8d966b0e6cf84fe191a72a58b8293b23.png', 'e1a9407cd0e937c4b411a93b7aca7c87', 1, 0);
INSERT INTO `sys_file` VALUES (19, 'Java3年经验开发个人简历模板.doc', 'doc', 47, 'http://localhost:9090/file/0e020e1337474a93ba3b43a75b2890ee.doc', '9ace4fac24420f85c753aa663009edb4', 1, 1);
INSERT INTO `sys_file` VALUES (20, '1626102561055-2.jpg', 'jpg', 24, 'http://localhost:9090/file/cd379f381364482aaad0d6ffb7209d3d.jpg', 'adb0481b283645af3809e3d8a1bdb294', 1, 1);
INSERT INTO `sys_file` VALUES (21, '1622011842830-5.jpg', 'jpg', 14, 'http://localhost:9090/file/7737484487db47ab89e58504ddf86ac1.jpg', '2dcd5d60c696c47fdfe0b482c18de0ea', 0, 1);
INSERT INTO `sys_file` VALUES (22, '1622536738094-7.jpg', 'jpg', 12, 'http://localhost:9090/file/ad5946fe27c14508ac796115f6465826.jpg', '35977e7dc2159a734f3c81de460e4b8d', 0, 1);
INSERT INTO `sys_file` VALUES (23, '用户信息 (1).xlsx', 'xlsx', 3, 'http://localhost:9090/file/02f70e07e69045c38b4748283ffeeabb.xlsx', '687f150737c967e74cfd6fa9ec981589', 0, 1);
INSERT INTO `sys_file` VALUES (24, '1641024983532cf.png', 'png', 634, 'http://localhost:9090/file/8d966b0e6cf84fe191a72a58b8293b23.png', 'e1a9407cd0e937c4b411a93b7aca7c87', 0, 1);
INSERT INTO `sys_file` VALUES (25, '1641024983532cf.png', 'png', 634, 'http://localhost:9090/file/8d966b0e6cf84fe191a72a58b8293b23.png', 'e1a9407cd0e937c4b411a93b7aca7c87', 0, 1);
INSERT INTO `sys_file` VALUES (26, '6.jpg', 'jpg', 30, 'http://localhost:9090/file/9b21a2b133b140e0bcd9bf66dc5cad1d.jpg', 'a2cf10bcbed5e9d98cbaf5467acae28d', 0, 1);
INSERT INTO `sys_file` VALUES (27, '9b21a2b133b140e0bcd9bf66dc5cad1d.jpg', 'jpg', 30, 'http://localhost:9090/file/9b21a2b133b140e0bcd9bf66dc5cad1d.jpg', 'a2cf10bcbed5e9d98cbaf5467acae28d', 0, 1);
INSERT INTO `sys_file` VALUES (28, '1622011842830-5.jpg', 'jpg', 14, 'http://localhost:9090/file/7737484487db47ab89e58504ddf86ac1.jpg', '2dcd5d60c696c47fdfe0b482c18de0ea', 1, 1);
INSERT INTO `sys_file` VALUES (29, '1641024983532cf (1).png', 'png', 634, 'http://localhost:9090/file/8d966b0e6cf84fe191a72a58b8293b23.png', 'e1a9407cd0e937c4b411a93b7aca7c87', 0, 1);
INSERT INTO `sys_file` VALUES (30, '1622011842830-5.jpg', 'jpg', 14, 'http://localhost:9090/file/7737484487db47ab89e58504ddf86ac1.jpg', '2dcd5d60c696c47fdfe0b482c18de0ea', 0, 1);
INSERT INTO `sys_file` VALUES (31, 'QQ截图20211214232106.jpg', 'jpg', 30, 'http://localhost:9090/file/7de0e50f915547539db12023cf997276.jpg', 'ba5dd263551a31d9bb0582b07cb480e1', 0, 1);
INSERT INTO `sys_file` VALUES (32, 'boot.jpg', 'jpg', 11, 'http://localhost:9090/file/657d7054d7864bd7a0aaba9e44f7924e.jpg', '2ab82ad78ff081665ee90f8cb38b45db', 0, 1);
INSERT INTO `sys_file` VALUES (33, 'QQ图片20210828212629.gif', 'gif', 188, 'http://localhost:9090/file/e5512c68a5614135a12064afa66c67e5.gif', 'ce524058758a83c046b97c66ddcb8a83', 0, 1);
INSERT INTO `sys_file` VALUES (34, 'vite.jpg', 'jpg', 27, 'http://localhost:9090/file/782f20b37b8b4a158c5e13a07fe826d5.jpg', 'c67bab9c32968cd0cda3e1608286b0d9', 0, 1);
INSERT INTO `sys_file` VALUES (35, 'mp.jpg', 'jpg', 32, 'http://localhost:9090/file/650e8330e78b4fed8fb0c1d866684b5d.jpg', 'cb887a9d64563352edce80cf50296cc5', 0, 1);
INSERT INTO `sys_file` VALUES (36, 'qq音乐.png', 'png', 445, 'http://localhost:9090/file/461504596ec040729776b674ddec88d3.png', '793fd534fa705475eb3358f68c87ec68', 0, 1);
INSERT INTO `sys_file` VALUES (37, 'QQ截图20211214232106.jpg', 'jpg', 30, 'http://localhost:9090/file/7de0e50f915547539db12023cf997276.jpg', 'ba5dd263551a31d9bb0582b07cb480e1', 0, 1);
INSERT INTO `sys_file` VALUES (38, 'boot.jpg', 'jpg', 11, 'http://localhost:9090/file/657d7054d7864bd7a0aaba9e44f7924e.jpg', '2ab82ad78ff081665ee90f8cb38b45db', 0, 1);
INSERT INTO `sys_file` VALUES (39, 'QQ截图20211214232106.jpg', 'jpg', 30, 'http://localhost:9090/file/7de0e50f915547539db12023cf997276.jpg', 'ba5dd263551a31d9bb0582b07cb480e1', 1, 0);
INSERT INTO `sys_file` VALUES (40, 'v1.mp4', 'mp4', 47484, 'http://localhost:9090/file/a22c9d62ef0648db86b9766bb14d742a.mp4', '3dda54bc1a07bd9112bfb381c20b4867', 0, 1);
INSERT INTO `sys_file` VALUES (41, '搜狗截图20220129174103.png', 'png', 56, 'http://localhost:9090/file/8567a00d2bf740e0a63794baf600cec3.png', '050df6119399582fda666834870608d7', 0, 1);
INSERT INTO `sys_file` VALUES (42, 'QQ图片20220307194920.png', 'png', 100, 'http://localhost:9090/file/5e40a867acd74d1f90b0ac9a765823e5.png', '0f1337b5c0ebf68f67718fcf42d1322f', 0, 1);
INSERT INTO `sys_file` VALUES (43, 'QQ图片20220307194920.png', 'png', 100, 'http://localhost:9090/file/5e40a867acd74d1f90b0ac9a765823e5.png', '0f1337b5c0ebf68f67718fcf42d1322f', 0, 1);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '路径',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '图标',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `pid` int NULL DEFAULT NULL COMMENT '父级id',
  `page_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '页面路径',
  `sort_num` int NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (2, '数据报表', '/dashbord', 'el-icon-s-marketing', '11', NULL, 'Dashbord', 100);
INSERT INTO `sys_menu` VALUES (4, '系统管理', NULL, 'el-icon-s-grid', NULL, NULL, NULL, 300);
INSERT INTO `sys_menu` VALUES (5, '用户管理', '/user', 'el-icon-user', NULL, 4, 'User', 301);
INSERT INTO `sys_menu` VALUES (6, '角色管理', '/role', 'el-icon-s-custom', NULL, 4, 'Role', 302);
INSERT INTO `sys_menu` VALUES (7, '菜单管理', '/menu', 'el-icon-menu', NULL, 4, 'Menu', 303);
INSERT INTO `sys_menu` VALUES (8, '文件管理', '/file', 'el-icon-document', NULL, 4, 'File', 304);
INSERT INTO `sys_menu` VALUES (9, '请我喝冰可乐', '/donate', 'el-icon-coffee\r\n', NULL, NULL, 'Donate', 200);
INSERT INTO `sys_menu` VALUES (10, '主页', '/home', 'el-icon-house', NULL, NULL, 'Home', 0);
INSERT INTO `sys_menu` VALUES (39, '课程管理', '/course', 'el-icon-menu', NULL, NULL, 'Course', 201);
INSERT INTO `sys_menu` VALUES (40, '高德地图', '/map', 'el-icon-house', NULL, NULL, 'Map', 999);
INSERT INTO `sys_menu` VALUES (41, '公告管理', '/article', 'el-icon-menu', NULL, NULL, 'Article', 999);
INSERT INTO `sys_menu` VALUES (42, '级联查询', '/building', 'el-icon-menu', NULL, NULL, 'Building', 999);
INSERT INTO `sys_menu` VALUES (43, '时间范围查询', '/timeSearch', 'el-icon-menu', NULL, NULL, 'TimeSearch', 999);
INSERT INTO `sys_menu` VALUES (44, '问卷管理', '/paperManage', 'el-icon-document', '问卷', 4, 'PaperManage', 2);
INSERT INTO `sys_menu` VALUES (45, '数量管理', '/max', 'el-icon-house', '数量', 4, '/max', 21);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `flag` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '唯一标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', '超级管理员', 'ROLE_ADMIN');
INSERT INTO `sys_role` VALUES (2, '用户', '用户', 'ROLE_STUDENT');
INSERT INTO `sys_role` VALUES (3, '管理员', '管理员', 'ROLE_TEACHER');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` int NOT NULL COMMENT '角色id',
  `menu_id` int NOT NULL COMMENT '菜单id',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '角色菜单关系表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 4);
INSERT INTO `sys_role_menu` VALUES (1, 5);
INSERT INTO `sys_role_menu` VALUES (1, 6);
INSERT INTO `sys_role_menu` VALUES (1, 7);
INSERT INTO `sys_role_menu` VALUES (1, 8);
INSERT INTO `sys_role_menu` VALUES (1, 10);
INSERT INTO `sys_role_menu` VALUES (1, 40);
INSERT INTO `sys_role_menu` VALUES (1, 41);
INSERT INTO `sys_role_menu` VALUES (1, 44);
INSERT INTO `sys_role_menu` VALUES (3, 2);
INSERT INTO `sys_role_menu` VALUES (3, 10);
INSERT INTO `sys_role_menu` VALUES (3, 39);
INSERT INTO `sys_role_menu` VALUES (3, 40);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '密码',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '昵称',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '电话',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '地址',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `ava` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '头像',
  `role` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '202cb962ac59075b964b07152d234b70', '管理员', 'admin@qq.com', '15308197564', '四川成都', '2022-01-22 21:10:27', 'http://localhost:9090/file/95058877bd844b31a5abd747b87df5f5.jpg', 'ROLE_ADMIN');
INSERT INTO `sys_user` VALUES (2, 'jiayou', '133b3752e52bae42230364cb720f81f7', 'jiayou', '1', '1', '1', '2023-06-07 19:01:34', 'http://localhost:9090/file/8b5507e6345945aca91e9e2be00e560d.jpg', 'ROLE_STUDENT');
INSERT INTO `sys_user` VALUES (16, '22222', '202cb962ac59075b964b07152d234b70', '猪八戒2', '2', '2', '高老庄', '2022-02-26 22:10:14', NULL, 'ROLE_TEACHER');
INSERT INTO `sys_user` VALUES (17, '333', '202cb962ac59075b964b07152d234b70', '孙悟空', '3', '3', '花果山', '2022-02-26 22:10:18', 'http://localhost:9090/file/7737484487db47ab89e58504ddf86ac1.jpg', 'ROLE_TEACHER');
INSERT INTO `sys_user` VALUES (18, 'nzz', '202cb962ac59075b964b07152d234b70', '哪吒', '2', '2', '钱塘江', '2022-03-29 16:59:44', '', 'ROLE_STUDENT');
INSERT INTO `sys_user` VALUES (19, 'yss', '202cb962ac59075b964b07152d234b70', '陈玄奘', '3', '3', '大唐', '2022-04-29 16:59:44', '', 'ROLE_STUDENT');
INSERT INTO `sys_user` VALUES (25, 'sir', '202cb962ac59075b964b07152d234b70', '白骨精', '21', '21', '大荒山', '2022-06-08 17:00:47', NULL, 'ROLE_STUDENT');
INSERT INTO `sys_user` VALUES (33, 'ew', '123', '白龙马', '21', '12', '西海', '2023-03-22 16:10:26', NULL, 'ROLE_TEACHER');
INSERT INTO `sys_user` VALUES (36, 'DAKM11', '1234', 'ajhn', '235ew', '1212', 'dsa', '2023-03-22 20:31:50', NULL, NULL);
INSERT INTO `sys_user` VALUES (42, '11', '202cb962ac59075b964b07152d234b70', '11', '11', '11', '11', '2023-06-02 17:12:16', NULL, 'ROLE_STUDENT');
INSERT INTO `sys_user` VALUES (43, '121', '4c56ff4ce4aaf9573aa5dff913df997a', '121', NULL, NULL, NULL, '2023-06-04 15:23:41', NULL, 'ROLE_STUDENT');
INSERT INTO `sys_user` VALUES (48, 'haha', '202cb962ac59075b964b07152d234b70', 'haha', NULL, NULL, NULL, '2023-06-13 16:11:35', NULL, 'ROLE_STUDENT');
INSERT INTO `sys_user` VALUES (49, 'yonghu', '81dc9bdb52d04dc20036dbd8313ed055', 'yonghu', '312@com', '123', '1245', '2023-06-18 16:40:33', 'http://localhost:9090/file/06487a8de2d54062a98f07a2fbfd7e52.jpg', 'ROLE_STUDENT');
INSERT INTO `sys_user` VALUES (51, '12', '202cb962ac59075b964b07152d234b70', '12', '12', '12', '12', '2023-06-18 16:54:39', NULL, 'ROLE_TEACHER');

-- ----------------------------
-- Table structure for sysfile
-- ----------------------------
DROP TABLE IF EXISTS `sysfile`;
CREATE TABLE `sysfile`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件类型',
  `size` bigint NULL DEFAULT NULL COMMENT '文件大小',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载链接',
  `isdelete` tinyint(1) NULL DEFAULT 0 COMMENT '是否删除',
  `enable` tinyint(1) NULL DEFAULT 1 COMMENT '是否禁用',
  `md5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysfile
-- ----------------------------
INSERT INTO `sysfile` VALUES (37, 'IMG_20230308_163938.jpg', 'jpg', 269, 'http://localhost:9090/file/21b4ab57308a4d25bec575abb3b17106.jpg', 0, 1, '49c819394539edf2cb85f64de794d925');
INSERT INTO `sysfile` VALUES (38, '1635073646141.jpeg', 'jpeg', 395, 'http://localhost:9090/file/dfc81c4f745d4038a249eb9b60693064.jpeg', 0, 1, '26237d556091d1d2df8eaa684dae3218');
INSERT INTO `sysfile` VALUES (39, 'yys_20221112191419.jpg', 'jpg', 54, 'http://localhost:9090/file/95058877bd844b31a5abd747b87df5f5.jpg', 1, 1, '4aee1cc7475564e4405ea62109786c45');
INSERT INTO `sysfile` VALUES (40, 'yys_20221112191419.jpg', 'jpg', 54, 'http://localhost:9090/file/95058877bd844b31a5abd747b87df5f5.jpg', 1, 1, '4aee1cc7475564e4405ea62109786c45');
INSERT INTO `sysfile` VALUES (41, 'yys_20221112191419.jpg', 'jpg', 54, 'http://localhost:9090/file/95058877bd844b31a5abd747b87df5f5.jpg', 1, 1, '4aee1cc7475564e4405ea62109786c45');
INSERT INTO `sysfile` VALUES (42, '胡彦斌-月光 (KTV版) (《秦时明月》动画主题曲)(高清).mp4', 'mp4', 14461, 'http://localhost:9090/file/86ba1ed4f5064dfc88a9959aad0a9f31.mp4', 1, 1, '9ffa560ff8f0f3dded4d69131035a936');
INSERT INTO `sysfile` VALUES (43, 'video_20230227_195009.mp4', 'mp4', 64305, 'http://localhost:9090/file/b0cdd76f3a8c44849c30eb0984c339a2.mp4', 1, 1, '9124848cf4c70e4e91d85ce3d029962a');
INSERT INTO `sysfile` VALUES (44, 'null6d3e23597a4029c1.jpg', 'jpg', 628, 'http://localhost:9090/file/afb357809db94f578e423e81ead35510.jpg', 1, 1, '71e4e8e7fbe057c77026754d761eb79e');
INSERT INTO `sysfile` VALUES (45, 'cmd.jpg', 'jpg', 66, 'http://localhost:9090/file/8b5507e6345945aca91e9e2be00e560d.jpg', 1, 1, '34252edaaea3ba5256fc7064c5c355a2');
INSERT INTO `sysfile` VALUES (46, 'cmd.jpg', 'jpg', 66, 'http://localhost:9090/file/8b5507e6345945aca91e9e2be00e560d.jpg', 0, 1, '34252edaaea3ba5256fc7064c5c355a2');
INSERT INTO `sysfile` VALUES (47, 'QQ图片20211003131005.jpg', 'jpg', 93, 'http://localhost:9090/file/3f7cadc0faf1436a87a30cdc02a3e18c.jpg', 0, 1, '432ad11e80a1f237c367f0fa44b6266c');
INSERT INTO `sysfile` VALUES (48, '作业1.html', 'html', 35, 'http://localhost:9090/file/262efad871d44ce5ba30a3586fbdd98a.html', 0, 1, '5ae1c155f83dbe3a0eceae2a80b62477');
INSERT INTO `sysfile` VALUES (49, '胡彦斌-月光 (KTV版) (《秦时明月》动画主题曲)(高清).mp4', 'mp4', 14461, 'http://localhost:9090/file/86ba1ed4f5064dfc88a9959aad0a9f31.mp4', 0, 1, '9ffa560ff8f0f3dded4d69131035a936');
INSERT INTO `sysfile` VALUES (50, 'cmd.jpg', 'jpg', 66, 'http://localhost:9090/file/8b5507e6345945aca91e9e2be00e560d.jpg', 0, 1, '34252edaaea3ba5256fc7064c5c355a2');
INSERT INTO `sysfile` VALUES (51, 'QQ图片20220315213804.jpg', 'jpg', 72, 'http://localhost:9090/file/06487a8de2d54062a98f07a2fbfd7e52.jpg', 0, 1, '47fd0b0b2cd6b93028428024ba015991');
INSERT INTO `sysfile` VALUES (52, 'QQ图片20220315213804.jpg', 'jpg', 72, 'http://localhost:9090/file/06487a8de2d54062a98f07a2fbfd7e52.jpg', 0, 1, '47fd0b0b2cd6b93028428024ba015991');
INSERT INTO `sysfile` VALUES (53, 'cmd.jpg', 'jpg', 66, 'http://localhost:9090/file/8b5507e6345945aca91e9e2be00e560d.jpg', 0, 1, '34252edaaea3ba5256fc7064c5c355a2');

-- ----------------------------
-- Table structure for t_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_comment`;
CREATE TABLE `t_comment`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '内容',
  `user_id` int NULL DEFAULT NULL COMMENT '评论人id',
  `time` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '评论时间',
  `pid` int NULL DEFAULT NULL COMMENT '父id',
  `origin_id` int NULL DEFAULT NULL COMMENT '最上级评论id',
  `article_id` int NULL DEFAULT NULL COMMENT '关联文章的id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_comment
-- ----------------------------
INSERT INTO `t_comment` VALUES (23, '大赛', 1, '2023-03-19 23:14:30', NULL, NULL, 4);
INSERT INTO `t_comment` VALUES (24, '大家好', 1, '2023-03-19 23:24:59', NULL, NULL, 5);
INSERT INTO `t_comment` VALUES (25, 'hello', 1, '2023-03-19 23:25:09', 24, 24, 5);
INSERT INTO `t_comment` VALUES (26, '八嘎', 28, '2023-03-19 23:32:45', NULL, NULL, 4);
INSERT INTO `t_comment` VALUES (27, '你好', 28, '2023-03-19 23:32:56', 23, 23, 4);
INSERT INTO `t_comment` VALUES (29, '666666', 1, '2023-06-06 16:48:21', NULL, NULL, 13);
INSERT INTO `t_comment` VALUES (30, 'fqffqq', 1, '2023-06-09 14:10:14', NULL, NULL, 13);
INSERT INTO `t_comment` VALUES (31, 'fqwF', 1, '2023-06-09 14:10:19', 30, 30, 13);
INSERT INTO `t_comment` VALUES (32, '666', 1, '2023-06-09 17:07:58', NULL, NULL, 15);
INSERT INTO `t_comment` VALUES (33, '666', 1, '2023-06-14 16:01:08', NULL, NULL, 11);
INSERT INTO `t_comment` VALUES (34, '666', 1, '2023-06-14 16:01:14', 33, 33, 11);
INSERT INTO `t_comment` VALUES (35, '666', 1, '2023-06-14 16:01:20', 34, 33, 11);
INSERT INTO `t_comment` VALUES (36, '666', 49, '2023-06-18 16:46:31', NULL, NULL, 6);
INSERT INTO `t_comment` VALUES (37, '你好', 49, '2023-06-18 16:46:44', 36, 36, 6);
INSERT INTO `t_comment` VALUES (40, '121234', 1, '2023-06-18 20:31:42', NULL, NULL, 6);

-- ----------------------------
-- Table structure for userha
-- ----------------------------
DROP TABLE IF EXISTS `userha`;
CREATE TABLE `userha`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of userha
-- ----------------------------
INSERT INTO `userha` VALUES (1, 'test666', '123456');
INSERT INTO `userha` VALUES (2, 'admin', '123456');
INSERT INTO `userha` VALUES (3, '1e2', '3216');

SET FOREIGN_KEY_CHECKS = 1;
